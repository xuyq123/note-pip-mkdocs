
## 笔记mkdocs

### 仓库

[gitlab]( https://gitlab.com/xuyq123/note-mkdocs ) &ensp; [gitee]( https://gitee.com/xy180/note-mkdocs ) &ensp; [github]( https://github.com/scott180/note-mkdocs ) &ensp; [gitlab_mkdocs]( https://xuyq123.gitlab.io/note-mkdocs/ )  &ensp; [github_mkdocs]( https://scott180.github.io/note-mkdocs/ ) 
	
### 文档

```
nav: 
  - 我的笔记: 我的笔记/note.md
  - 书法练习:
    - 书法练习轨迹ReadMe: 书法练习/书法练习轨迹ReadMe.md
    - 书法练习轨迹--明月几时有: 书法练习/书法练习轨迹--明月几时有.md
    - 笔名汉字频率分析: 书法练习/笔名汉字频率分析.md
    - 古文诗词: 书法练习/古文诗词.md
    - 多宝塔碑: 书法练习/多宝塔碑.md
  - java资料:
    - java: java资料/java.md
    - gitNote: java资料/gitNote.md
    - linuxNote: java资料/linuxNote-x.md

```

---
	
> `文徵明-小楷赤壁赋` <br/>
![文徵明-小楷赤壁赋]( https://xyqin.coding.net/p/my/d/imgs/git/raw/master/other/文徵明-小楷赤壁赋.jpg)

